 $(window).on('load resize', function () { 
		var wHeight = $( window ).height() ; 
		var sHeight = $('.sidebar').height();
		whatpodHeight = wHeight -  sHeight;
	 $('.whats-new-pod').height(whatpodHeight); 
	  $('.whats-new-pod ul').slimScroll({
          height: whatpodHeight - 60,
          width: '235px'
      });
	  
	  
	  
		 
});



$(document).ready(function(){
 $(".right-sidebar ").hover(function(){
     $('.user-sec').toggleClass("toggle-img");
    });
	
	$(".family").hover(function(){
     $('.family-dinners').toggleClass("active");
    });
	$(".health").hover(function(){
     $('.try-cycling').toggleClass("active");
    });
	
		$('.family-dinners').hover(
       function(){ $('.family').addClass('active') },
       function(){ $('.family').removeClass('active') }
		)

		$('.try-cycling').hover(
			   function(){ $('.health').addClass('active') },
			   function(){ $('.health').removeClass('active') }
		)
		
		$(".search-icon").hover(function(){
     $('.top-search, .search-box').addClass("active");
    });
		
		
    
});

$(document).click(function() {
        $('.top-search, .search-box').removeClass('active'); //make all inactive
    });


// Textarea Auto Height
jQuery.each(jQuery('textarea[data-autoresize]'), function() {
    var offset = this.offsetHeight - this.clientHeight;
 
    var resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
    };
    jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
});




// Exercise 3 Step 6 Question and Answers Collapse 




$(function() {
  var transition = false;
  var $active = true;

  $('.quesOneRight').on('click', function() {
    if(!$active) {
      $active = true;
      $('#questionOne .panel-collapse').collapse('hide');
      
    } else {
      $active = false;
      if($('#questionOne .panel-collapse.in').length){
        transition = true;
      }
      else{
      	$('#questionOne .panel-collapse').collapse('show');
      }
    }
    setTimeout(function(){
  	    $('.quesOneRight').prop('disabled','');
    },800);

  });

   $('.quesTwoRight').on('click', function() {
    if(!$active) {
      $active = true;
      $('#questionTwo .panel-collapse').collapse('hide');
      
    } else {
      $active = false;
      if($('#questionTwo .panel-collapse.in').length){
        transition = true;
      }
      else{
      	$('#questionTwo .panel-collapse').collapse('show');
      }
    }
    setTimeout(function(){
  	    $('.quesTwoRight').prop('disabled','');
    },800);
  });


 	$('.quesThreeRight').on('click', function() {
    if(!$active) {
      $active = true;
      $('#questionThree .panel-collapse').collapse('hide');
      
    } else {
      $active = false;
      if($('#questionThree .panel-collapse.in').length){
        transition = true;
      }
      else{
      	$('#questionThree .panel-collapse').collapse('show');
      }
    }
    setTimeout(function(){
  	    $('.quesThreeRight').prop('disabled','');
    },800);
  });

 	$('.quesfourRight').on('click', function() {
    if(!$active) {
      $active = true;
      $('#questionfour .panel-collapse').collapse('hide');
      
    } else {
      $active = false;
      if($('#questionfour .panel-collapse.in').length){
        transition = true;
      }
      else{
      	$('#questionfour .panel-collapse').collapse('show');
      }
    }
    setTimeout(function(){
  	    $('.quesfourRight').prop('disabled','');
    },800);
  });


 	$('#quesFourRight').on('click', function() {
    if(!$active) {
      $active = true;
      $('#question4 .panel-collapse').collapse('hide');
      
    } else {
      $active = false;
      if($('#question4 .panel-collapse.in').length){
        transition = true;
      }
      else{
      	$('#question4 .panel-collapse').collapse('show');
      }
    }
    setTimeout(function(){
  	    $('#quesFourRight').prop('disabled','');
    },800);
  });


$('.excertwoStep8Ques a').click(function (e) {
        e.preventDefault();
        $(this).closest('a').addClass('active').siblings().removeClass('active');

        
    });

});


// Smooth Scrolling to top

 jQuery.fn.anchorAnimate = function(settings) {     
        
    settings = jQuery.extend({
            speed : 500     
        }, settings);        
        
        return this.each(function(){        
            var caller = this         
                $(caller).click(function (event) {               
                event.preventDefault()            
                var locationHref = window.location.href             
                var elementClick = $(caller).attr("href")             
                var destination = $(elementClick).offset().top - 100;             
            $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, settings.speed, function() {    
                //window.location.hash = elementClick            
                    });             
                return false;        
            })     
                }) 
                };

 $(document).ready(function () {
				$(document).on("scroll", onScroll);

$(".excertwoStep8Ques a").anchorAnimate(); 

				$('.excertwoStep8Ques a[href^="#"]').on('click', function (e) {
					e.preventDefault();
					$(document).off("scroll");

					$('.excertwoStep8Ques a').each(function () {
						$(this).removeClass('active');
					})
					$(this).addClass('active');

					var target = this.hash; 
					$target = $(target);
					$('html, body').stop().animate({
						'scrollTop': $target.offset().top - 70
					}, 500, 'swing', function () {
						window.location.hash = target;
						$(document).on("scroll", onScroll);
					});
				});
			});

			function onScroll(event){
				var scrollPosition = $(document).scrollTop();
				$('.excertwoStep8Ques a').each(function () {
					var currentLink = $(this);
					var refElement = $(currentLink.attr("href"));
					if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
						$('.excertwoStep8Ques a').removeClass("active");
						currentLink.addClass("active");
					}
					else{
						currentLink.removeClass("active");
					}
				});
			}



$('#quesOneSlider').slider({
  formatter: function(value) {
    return 'Current value: ' + value;
  }
});

